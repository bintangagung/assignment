package animal;

public class Parrots extends Animal{

    public Parrots(String name, int bodyLength){
        super(name, bodyLength, true);
    }

    public void conversation(String youSay){
        System.out.println(this.name + " says: " + youSay.toUpperCase());
    }

    public void fly(){
        System.out.println("Parrots " + this.name + " flies!");
        System.out.println(this.name + " makes a voice: FLYYYY....");
    }

    public void doNothing(){
        System.out.println("You do nothing...");
    }
}