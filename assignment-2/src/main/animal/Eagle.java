package animal;

public class Eagle extends Animal{
    
    public Eagle(String name, int bodyLength){
        super(name, bodyLength, false);
    }

    public void fly(){
        System.out.println("Parrot " + this.name + " flies!");
        System.out.println(this.name + " makes a voice: kwaakk...");
        System.out.println("You hurt!");
    }

    public void doNothing(){
        System.out.println("You do nothing...");
    }
}