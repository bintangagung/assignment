package animal;
import java.util.Random;

public class Cat extends Animal {
    Random acak = new Random();

    public Cat(String name, int bodyLength){
        super(name, bodyLength, true);
    }

    public void bersuara(){
        int i = acak.nextInt(4); // merandom sebanyak 4 kali
        String suara = "";
        if(i == 0) suara = "Miaaaw..";
        else if(i == 1) suara = "Purrr..";
        else if(i == 2) suara = "Mwaw!";
        else suara = "Mraaawr!";
        System.out.println(this.name + " makes a voice: " + suara);
    }

    public void brush(){
        System.out.println("Time to clean " + this.name + " fur");
        System.out.println(this.name + " makes a voice: Nyaaan...");
    }

    public void doNothing(){
        System.out.println("You do nothing...");
    }
}