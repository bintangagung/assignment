package animal;

public class Lion extends Animal{

    public Lion(String name, int bodyLength){
        super(name, bodyLength, false);
    }

    public void hunting(){
        System.out.println("Lion is hunting..");
        System.out.println(this.name + " makes a voice: err...!");
    }

    public void brush(){
        System.out.println("Clean the lion’s mane..");
        System.out.println(this.name + " makes a voice: Hauhhmm!");
    }

    public void disturb(){
        System.out.println(this.name + " makes a voice: HAUHHMM!");
    }

    public void doNothing(){
        System.out.println("You do nothing...");
    }
}