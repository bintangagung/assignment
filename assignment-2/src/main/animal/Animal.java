package animal;

public class Animal{
    protected String name;
    protected int bodyLength;
    protected int length; //untuk ukuran kandang
    protected int width; //untuk ukuran kandang
    protected boolean type; //tipe jika true = indoor, kalo false = outdoor

    public Animal(String name, int bodyLength, boolean type){
        this.name = name;
        this.bodyLength = bodyLength;
        this.type = type;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getBodyLength(){
        return this.bodyLength;
    }

    public void setBodyLength(int bodyLength){
        this.bodyLength = bodyLength;
    }

    public boolean getType(){
        return this.type;
    }

    public void setType(boolean type){
        this.type = type;
    }
}