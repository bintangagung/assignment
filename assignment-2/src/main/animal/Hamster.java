package animal;

public class Hamster extends Animal{

    public Hamster(String name, int bodyLength){
        super(name, bodyLength, true);
    }
    
    public void gnaw(){
        System.out.println(this.name + " makes a voice: Ngkkrit.. Ngkkrrriiit");
    }

    public void run(){
        System.out.println(this.name + " makes a voice: Trrr... Trrr...");
    }

    public void doNothing(){
        System.out.println("You do nothing...");
    }
}