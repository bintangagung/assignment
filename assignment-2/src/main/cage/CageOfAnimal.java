package cage;
import animal.*;

public class CageOfAnimal{
    private String tipe;
    private String area;
    private Animal binatang;
    private String category;

    public CageOfAnimal(Animal binatang){
        this.binatang = binatang;
        setType();
    }

    public void setType(){
        if(binatang.getType() == true){
            category = "indoor";
            if(binatang.getBodyLength() < 45){
                area = "60cm x 60cm";
                tipe = "A";
            } else if(binatang.getBodyLength() < 60){
                area = "90cm x 60cm";
                tipe = "B";
            } else if(binatang.getBodyLength() >= 60){
                area = "120cm x 60cm";
                tipe = "C";
            }
        } else{
            category = "outdoor";
            if(binatang.getBodyLength() < 75){
                area = "120cm x 150cm";
                tipe = "A";
            } else if(binatang.getBodyLength() < 90){
                area = "120cm x 150cm";
                tipe = "B";
            } else if(binatang.getBodyLength() > 90){
                area = "120cm x 180cm";
                tipe = "C";
            }
        }
    }

	public String getTipe() {
		return tipe;
	}

	public String getArea() {
		return area;
    }
    
	public String getCategory() {
		return category;
    }
    
    public Animal getBinatang(){
        return this.binatang;
    }
}