package cage;
import java.util.ArrayList;

public class CageOfCage{
    
    private ArrayList<CageOfAnimal> level1 = new ArrayList<CageOfAnimal>();
    private ArrayList<CageOfAnimal> level2 = new ArrayList<CageOfAnimal>();
    private ArrayList<CageOfAnimal> level3 = new ArrayList<CageOfAnimal>();
	private int size;

    public CageOfCage(int size){
        this.size = size;
    }

    public void grouping(CageOfAnimal kandang, int i){
        int temp = size/3;
        if(temp == 0) temp += 1;
        if(size%3 <= 1){
            if(i < temp){
                level1.add(kandang);
            } else if(i < 2*temp){
                level2.add(kandang);
            } else{
                level3.add(kandang);
            }
        } else{
            if(i < temp){
                level1.add(kandang);
            } else if(i < 2*temp){
                level2.add(kandang);
            } else{
                level3.add(kandang);
            }
        }   
    }

    public CageOfAnimal findAnimal(String name){
        CageOfAnimal cek = null;
        ArrayList<CageOfAnimal> level = null;
        for(int i = 1; i <= 3; i++){
            if(i == 1) level = level1;
            else if(i == 2) level = level2;
            else if(i == 3) level = level3;
            for (CageOfAnimal var : level) {
                if(name.equals(var.getBinatang().getName()))
                    cek = var;
            }
        }
        return cek;
    }

    public ArrayList<CageOfAnimal> getLevel1(){
        return level1;
    }

    public void setLevel1(ArrayList<CageOfAnimal> level1){
        this.level1 = level1;
    }

    public ArrayList<CageOfAnimal> getLevel2(){
        return level2;
    }

    public void setLevel2(ArrayList<CageOfAnimal> level2){
        this.level2 = level2;
    }

    public ArrayList<CageOfAnimal> getLevel3(){
        return level3;
    }

    public void setLevel3(ArrayList<CageOfAnimal> level3){
        this.level3 = level3;
    }

    public int getSize(){
        return this.size;
    }
}