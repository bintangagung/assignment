package cage;

import java.util.ArrayList;

public class CageArranger{
    public static void arrange(CageOfCage kelompokKandang){
        // mengambil tipe dari kelompok hewan tersebut in/out(door)
        if(kelompokKandang.getLevel1().size() > 0) 
            System.out.println("location: " + kelompokKandang.getLevel1().get(0).getCategory());
        cetak(kelompokKandang);

        // memposisikan perpindahan level
        ArrayList<CageOfAnimal> temp = kelompokKandang.getLevel1();
        kelompokKandang.setLevel1(kelompokKandang.getLevel3());
        kelompokKandang.setLevel3(kelompokKandang.getLevel2());
        kelompokKandang.setLevel2(temp);
        
        // merubah index dari tiap level
        for(int i = 0; i < 3; i++){
            ArrayList<CageOfAnimal> tempKelompok = null;
            if(i == 0) tempKelompok = kelompokKandang.getLevel1();
            else if(i == 1) tempKelompok = kelompokKandang.getLevel2();
            else if(i == 2) tempKelompok = kelompokKandang.getLevel3();

            for(int j = 0; j < tempKelompok.size() / 2; j++){
                CageOfAnimal tempCage = temp.get(j);
                tempKelompok.set(j, tempKelompok.get(tempKelompok.size() - 1 - j));
                tempKelompok.set(tempKelompok.size() - j - 1, tempCage);
            }
        }
        System.out.println("After rearrangement...");
        cetak(kelompokKandang);
    }

    public static void cetak(CageOfCage kelompokKandang){
        for(int i = 3; i >= 1; i--){
            ArrayList<CageOfAnimal> tempKelompok = null;
            if(i == 1) tempKelompok = kelompokKandang.getLevel1();
            else if(i == 2) tempKelompok = kelompokKandang.getLevel2();
            else if(i == 3) tempKelompok = kelompokKandang.getLevel3();

            System.out.print("level " + i + ": ");
            for (CageOfAnimal var : tempKelompok) { 
                String nama = var.getBinatang().getName();
                int panjang = var.getBinatang().getBodyLength();
                String tipe = var.getTipe();
                System.out.printf("%s (%d - %s), ", nama, panjang, tipe);
            }
            System.out.println();
        }
        System.out.println();
    }
}