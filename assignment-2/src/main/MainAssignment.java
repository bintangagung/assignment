import java.util.Scanner;
import animal.*;
import cage.*;

public class MainAssignment{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] listAnimal = {"cat", "eagle", "hamster", "parrots", "lion"};
        CageOfCage[] cages  = new CageOfCage[listAnimal.length];     

        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        
        for(int i = 0; i < 5; i++){
            System.out.print(listAnimal[i] + ": ");
            int jumlahHewan = Integer.parseInt(in.nextLine());
            cages[i] = new CageOfCage(jumlahHewan);

            if(jumlahHewan > 0){
                System.out.println("Provide the information of " + listAnimal[i] + "(s): ");
                String[] info = in.nextLine().split(",");
                for(int j = 0; j < jumlahHewan; j++){
                    Animal hewan = makeObject(info[j], listAnimal[i]);
                    CageOfAnimal kandang = new CageOfAnimal(hewan);
                    cages[i].grouping(kandang, j);
                }
            }
        }
        System.out.println("Animals have been successfully recorded!");
        System.out.println();

        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        for(int i = 0; i < 5; i++){
            if(cages[i].getSize() != 0)
                CageArranger.arrange(cages[i]);
        }
        
        System.out.println("NUMBER OF ANIMALS:");
        for(int i = 0; i < 5; i++){
            System.out.println(listAnimal[i] + ":" + cages[i].getSize());
        }
        System.out.println();

        System.out.println("=============================================");
        while (true){
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrots, 5: Lion, 99: Exit)");
            int pilihan = Integer.parseInt(in.nextLine());
            if(pilihan == 99){
                break;
            }
            System.out.print("Mention the name of " + listAnimal[pilihan-1] + " you want to visit: ");
            String namaHewan = in.nextLine();
            if(cages[pilihan -1].findAnimal(namaHewan) == null){
                System.out.println("There is no cat with that name! Back to the office!");
            } else{
                System.out.println("You are visiting " + namaHewan + "(" +
                listAnimal[pilihan-1] + ")" + " now, what would you like to do?");
                process(listAnimal[pilihan-1], cages[pilihan-1], namaHewan);
                System.out.println("Back to the office!\n");
            }
        }
        in.close();
    }

    // method untuk membuat object sesuai dengan apa yang diminta  dengan slicing dulu
    public static Animal makeObject(String x, String y){
        String[] temp = x.split("\\|");
        int temp1 = Integer.parseInt(temp[1]);
        if(y.equals("cat")){
            Animal kucing = new Cat(temp[0], temp1);
            return kucing;
        } else if(y.equals("eagle")){
            Animal elang = new Eagle(temp[0], temp1);
            return elang;
        } else if(y.equals("hamster")){
            Animal curut = new Hamster(temp[0], temp1);
            return curut;
        } else if(y.equals("parrots")){
            Animal beo = new Parrots(temp[0], temp1);
            return beo;
        } else if(y.equals("lion")){
            Animal singa = new Lion(temp[0], temp1);
            return singa;
        }
        return null;
    }

    // untuk meminta apa yang mau dilakukan user terhadap satuan hewan 
    public static void process(String x, CageOfCage y, String name){
        Scanner in = new Scanner(System.in);
        Animal tempAnimal = y.findAnimal(name).getBinatang();

        if(x.equals("cat")){
            System.out.println("1: Brush the fur 2: Cuddle");
            int command = Integer.parseInt(in.nextLine());
            if(command == 1) ((Cat)tempAnimal).brush();
            else if(command == 2) ((Cat)tempAnimal).bersuara();
            else ((Cat)tempAnimal).doNothing();
        } else if(x.equals("eagle")){
            System.out.println("1: Order to fly");
            int command = Integer.parseInt(in.nextLine());
            if(command == 1) ((Eagle)tempAnimal).fly();
            else ((Eagle)tempAnimal).doNothing();
        } else if(x.equals("hamster")){
            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
            int command = Integer.parseInt(in.nextLine());
            if(command == 1) ((Hamster)tempAnimal).gnaw();
            else if(command == 2) ((Hamster)tempAnimal).run();
            else ((Hamster)tempAnimal).doNothing();
        } else if(x.equals("parrots")){
            System.out.println("1: Order to fly 2: Do conversation");
            int command = Integer.parseInt(in.nextLine());
            if(command == 1) ((Parrots)tempAnimal).fly();    
            else if(command == 2){
                System.out.print("You say: ");
                String youSay = in.nextLine();
                ((Parrots)tempAnimal).conversation(youSay);
            } else ((Parrots)tempAnimal).doNothing();
        } else if(x.equals("lion")){
            System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
            int command = Integer.parseInt(in.nextLine());
            if(command == 1) ((Lion)tempAnimal).hunting();
            else if(command == 2) ((Lion)tempAnimal).brush();
            else if(command == 3) ((Lion)tempAnimal).disturb();
            else ((Lion)tempAnimal).doNothing();
        }
    }

}