import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import java.awt.*;

public class Deck {
    private JLabel label;
    private JLabel welcomeMsg;
    private int tryCounter = 0;
    private int matchCounter = 0;
    private JButton card1 = null;
    private JButton card2 = null;
    private JPanel deckPanel = new JPanel();
    private JPanel otherPanel = new JPanel();
    private JFrame board = new JFrame("Match-Pair Memory Game by Bintang");
    private ArrayList<ImageIcon> cardContent = new ArrayList<>();
    private ArrayList<JButton> cardList = new ArrayList<>();
    GridBagConstraints layoutOptimize = new GridBagConstraints();


    public Deck(){                                                 //Constructor membuat deck
        board.setLayout(new GridBagLayout());
        deckMaker();
        stuffMaker();

        layoutOptimize.gridx = 0;
        layoutOptimize.gridy = 0;
        layoutOptimize.fill = GridBagConstraints.BOTH;

        board.add(deckPanel, layoutOptimize);
        layoutOptimize.gridy = 1;
        board.add(otherPanel, layoutOptimize);

        board.setVisible(true);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.pack();
    }


    private void deckMaker() {                                      //membuat deck sesuai ukuran 6x6
        deckPanel.setLayout(new GridLayout(6, 6));

        for(int i = 1; i <= 18; i++){
            ImageIcon icon = new ImageIcon("gambar/" + i + ".jpg"); //mengakses gambar
            cardContent.add(icon);
            cardContent.add(icon);
        }
        Collections.shuffle(cardContent);
        
        for(int i = 0; i < 36; i++){
            JButton button = new JButton(new ImageIcon("gambar/bintang.jpg"));
            button.setBackground(Color.WHITE);
            cardList.add(button);
            button.setDisabledIcon(cardContent.get(i));
            button.addActionListener(e -> pressButton(button));     //lamda expresion button
            deckPanel.add(button);
        }
    }

    public void stuffMaker(){                                      //reset, keluar dan percobaan
        otherPanel.setLayout(new GridBagLayout());

        JButton resetBt = new JButton("Reset");
        resetBt.addActionListener(e -> reset());
        layoutOptimize.gridx = 0;
        layoutOptimize.gridy = 0;
        otherPanel.add(resetBt, layoutOptimize);

        JButton exitBt = new JButton("Keluar");
        exitBt.addActionListener(e -> System.exit(0));
        layoutOptimize.gridx = 1;
        otherPanel.add(exitBt, layoutOptimize);

        label = new JLabel("Percobaan: " + tryCounter);
        layoutOptimize.gridy = 1;
        otherPanel.add(label, layoutOptimize);
    }

    public void pressButton(JButton Press){                        //button akan dipencet jika kartu masih bisa dipencet 
        Press.setEnabled(false);                                    
        if(card1 == null && card2 == null)
            card1 = Press;
        else if(card1 != null && card2 == null){
            card2 = Press;
            tryCounter +=1;
            label.setText("Percobaan: " + tryCounter);
            imageChecker();
            checkWin();
        }else if(card1 != null && card2 != null){
            card1.setEnabled(true);
            card2.setEnabled(true);
            card1 = null;
            card2 = null;
            card1 = Press;
        }
    }

    public void checkWin(){                                //cek win
        if(matchCounter == 36){
            JOptionPane.showMessageDialog(board, "You Win!");
        }
    }

    public void imageChecker(){                            //cek kesamaan gambar
        if(card1.getDisabledIcon().equals(card2.getDisabledIcon())){
            card1.setEnabled(false);
            card2.setEnabled(false);
            card1.setVisible(false);
            card2.setVisible(false);
            matchCounter += 2;
            card1 = null;
            card2 = null;
        }
    }
     
    public void reset(){                                   //reset game dengan mengubah value instance variable
        for(JButton i: cardList){
            i.setVisible(true);
            i.setEnabled(true);
        }
        matchCounter = 0;
        tryCounter = 0;
        label.setText("Percobaan: " + tryCounter);
    }
}
