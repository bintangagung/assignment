Hewan baru datang ke Taman Javari! Ada ular langka dari Afrika
dan paus pintar dari Kiluan. Manajemen Taman Javari berencana untuk membuat
beberapa bagian yang berbeda untuk setiap jenis binatang ketika mereka menjadi lebih banyak
berbagai. Bagian yang mereka rencanakan untuk buat adalah "Jelajahi Mamalia",
"World of Aves" (burung), dan "Kerajaan Reptilian". Karena Anda seorang
programmer yang bekerja di Javari Park, Anda perlu memperbarui yang sebelumnya Anda buat
program. Sekarang, setiap hewan harus diklasifikasikan menjadi tiga besar
bagian.

Javari Park memiliki atraksi hewan setiap minggu. Berikut ini adalah detail dari
atraksi binatang:
Lingkaran Api: Atraksi ini dapat dilakukan oleh singa, ikan paus, atau elang
Dancing Animals: Atraksi ini dapat dilakukan oleh burung beo, ular, kucing,
atau hamster
Menghitung Tuan: Daya tarik ini dapat dilakukan oleh hamster, paus, atau
burung beo
Passionate Coders: Atraksi ini dapat dilakukan oleh hamster, kucing, atau
ular

Manajemen menginginkan informasi yang jelas dari masing-masing kelas binatang yang ada
disimpan dalam program Anda. Anda perlu memastikan bahwa nama, ID (ya, ID mereka
unik untuk setiap hewan), panjang badan, berat badan, jenis kelamin, dan tipenya
dicatat

Hanya hewan dalam kondisi tertentu yang berhak melakukan atraksi. Secara umum,
hanya hewan yang sehat yang dapat melakukan atraksi. Beberapa lainnya, lebih spesifik
ketentuannya adalah:
Mamalia hanya bisa melakukan atraksi jika mereka tidak hamil
Aves hanya bisa melakukan atraksi jika mereka tidak bertelur
Hanya singa jantan yang bisa melakukan atraksi
Hanya reptil yang jinak yang dapat melakukan atraksi

Jadi, kali ini Anda diminta untuk membuat program pendaftaran yang bisa
digunakan oleh pengunjung di Javari Park. Para pengunjung dapat menggunakan program ini untuk mendapatkannya
informasi tentang atraksi yang akan datang di Javari Park. Program perlu
dapat membaca data hewan dari file dan menghasilkan daftar hewan
Format JSON. Program ini harus dikembangkan oleh
mengikuti prinsip pemrograman berorientasi objek yang baik dan pastikan itu bisa
membantu para pengunjung untuk mendaftar ke Javari Park Festival!