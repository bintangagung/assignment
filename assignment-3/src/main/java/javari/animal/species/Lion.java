package javari.animal.species;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.Mammals;

public class Lion extends Mammals {
    public Lion(Integer id, String nama, Gender gender, double length,
                double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, nama, gender, length, weight, specialCondition, condition);
    }

    protected boolean specificCondition() {
        return super.specificCondition() && this.body.getGender() == Gender.MALE;
    }
}
