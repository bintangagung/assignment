package javari.animal.species;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.Aves;

public class Parrot extends Aves {
    public Parrot(Integer id, String nama, Gender gender, double length,
                  double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, nama, gender, length, weight, specialCondition, condition);
    }
}
