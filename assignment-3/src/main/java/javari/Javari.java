package javari;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.Visitor;
import javari.reader.AnimalAttractionsReader;
import javari.reader.AnimalCategoriesReader;
import javari.reader.AnimalRecordsReader;
import javari.reader.CsvReader;
import javari.repository.AttractionsRepository;
import javari.repository.CategoriesRepository;
import javari.repository.RecordRepository;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Javari {
    static private Scanner in = new Scanner(System.in);

    public static void main(String args[]) {


        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");

        CsvReader[] CsvFiles = new CsvReader[3];
        String textPath = System.getProperty("user.dir") + "\\assignment-3\\src\\main\\java\\javari\\input";
        while (true) {
            try {
                CsvFiles[0] = new AnimalCategoriesReader(Paths.get(textPath, "animals_categories.csv"));
                CsvFiles[1] = new AnimalAttractionsReader(Paths.get(textPath, "animals_attractions.csv"));
                CsvFiles[2] = new AnimalRecordsReader(Paths.get(textPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } 
catch (IOException e) {
                System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                textPath = in.nextLine();
                textPath = textPath.replace("\\", "\\\\");
            }
        }
        CsvFiles[0].countValidRecords();
        CsvFiles[1].countValidRecords();
        CsvFiles[2].countValidRecords();

        System.out.println("Found "+CategoriesRepository.findNumberOfSection()
        +" valid sections and "+CsvFiles[0].countInvalidRecords()+" invalid sections");
        System.out.println("Found "+AttractionsRepository.findNumberOfAttractions()
        +" valid attractions and "+CsvFiles[1].countInvalidRecords()+" invalid attractions");
                System.out.println("Found "+CategoriesRepository.findNumberOfCategories()
        +" valid animal categories and "+CsvFiles[0].countInvalidRecords()+" invalid animal categories");
        System.out.println("Found "+RecordRepository.getNumberOfAnimals()
        +" valid animal records and "+CsvFiles[2].countInvalidRecords()+" invalid animal records");
        registrationService();
    }

    private static void registrationService() {
        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n" +
                "\n" +
                "Please answer the questions by typing the number. Type # if you want to return to the previous menu\n" +
                "\n" +
                "Javari Park has 3 sections:");
        List<String> sections = CategoriesRepository.findAllSection();
        for (int i = 1; i <= sections.size(); ++i) {
            System.out.println(i + ". " + sections.get(i - 1));
        }
        System.out.print("Please choose your preferred section (type the number): ");
        sectionService();
    }

    private static void sectionService() {
        String input = in.nextLine();
        if (input.equals("#")) {
            registrationService();
        } else {
            int sectionNumber = Integer.parseInt(input);
            String section = CategoriesRepository.findSectionById(sectionNumber);
            List<String> speciesInSection = CategoriesRepository.findAllSpeciesBySection(section);

            System.out.println("\n--" + section + "--");
            for (int i = 1; i <= speciesInSection.size(); ++i) {
                System.out.println(i + ". " + speciesInSection.get(i - 1));
            }
            System.out.print("Please choose your preferred animals (type the number): ");
            attractionService(speciesInSection);
        }
    }

    private static void attractionService(List<String> speciesInSection) {
        String input = in.nextLine();
        if (input.equals("#")) {
            registrationService();
        } else {
            int speciesNumber = Integer.parseInt(input);
            String speciesName = speciesInSection.get(speciesNumber - 1);
            List<String> attractions = AttractionsRepository.findAttractionsBySpecies(speciesName);
            System.out.println("\n---" + speciesName + "---\n" +
                    "Attractions by " + speciesName + ":");
            for (int i = 1; i <= attractions.size(); ++i) {
                System.out.println(i + ". " + attractions.get(i - 1));
            }
            System.out.print("Please choose your preferred attractions (type the number): ");
            finalService(speciesName, attractions);
        }
    }

    private static void finalService(String speciesName, List<String> attractions) {

        String input = in.nextLine();
        if (input.equals("#")) {
            sectionService();
        } else {

            int attractionNumber = Integer.parseInt(input);
            String attraction = attractions.get(attractionNumber - 1);
            System.out.println("Wow, one more step,");
            System.out.println("please let us know your name: ");
            
            String name = in.nextLine();
            System.out.println();
            System.out.println("Yeay, final check!");
            System.out.println("Here is your data, and the attraction you chose:");
            System.out.println("Name: " + name);
            System.out.println("Attractions: " + attraction + " -> " + speciesName);
            System.out.println("With: " + RecordRepository.getAnimalsNameBySpeciesAsStrings(speciesName));
            System.out.println("Is the data correct? (Y/N): ");
            String response = in.nextLine();
            if (response.equals("N")) {
                registrationService();
            } else {
                System.out.println("Thank you for your interest. " +
                        "Would you like to register to other attractions? (Y/N): ");
                response = in.nextLine();
                if (response.equals("Y")) {
                    registrationService();
                } else {
                    //json part
                    Registration visitor = Visitor.findVisitor(name);
                    if (visitor == null) {
                        visitor = new Visitor(name);
                    }
                    SelectedAttraction selectedAttraction = new Attraction(attraction, speciesName);
                    for (Animal animal : RecordRepository.getAnimalsBySpecies(speciesName)) {
                        selectedAttraction.addPerformer(animal);
                    }
                    visitor.addSelectedAttraction(selectedAttraction);

                    String textPath = System.getProperty("user.dir") + "\\assignment-3\\src\\main\\java\\javari\\json";
                    try {
                        RegistrationWriter.writeJson(visitor, Paths.get(textPath));
                    } catch (IOException e) {
                        System.out.println("Something went wrong with " + visitor.getVisitorName());
                    }
                }
            }
        }
    }

}
