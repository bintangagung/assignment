package javari.reader;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.*;
import javari.animal.species.*;
import javari.repository.RecordRepository;

import java.io.IOException;
import java.nio.file.Path;

public class AnimalRecordsReader extends CsvReader {
    public AnimalRecordsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        long ret = 0;
        for (String line : lines) {
            Animal animal;
            try {
                switch (line.split(",")[1]) {
                    case "Lion":
                        animal = new Lion(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Mammals.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Cat":
                        animal = new Cat(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Mammals.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Eagle":
                        animal = new Eagle(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Aves.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Parrot":
                        animal = new Parrot(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Aves.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Snake":
                        animal = new Snake(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Reptiles.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Whale":
                        animal = new Whale(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Mammals.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );
                        break;
                    case "Hamster":
                        animal = new Hamster(Integer.parseInt(line.split(",")[0]),
                                line.split(",")[2],
                                Gender.parseGender(line.split(",")[3]),
                                Double.parseDouble(line.split(",")[4]),
                                Double.parseDouble(line.split(",")[5]),
                                Mammals.SpecialCondition.parseSpecialCondition(line.split(",")[6]),
                                Condition.parseCondition(line.split(",")[7])
                        );

                        break;
                    default:
                        throw new UnsupportedOperationException();
                }
                RecordRepository.save(animal);
            } catch (Exception e) {
                //do nothing
            }
        }
        return RecordRepository.getNumberOfAnimals();
    }

    public long countInvalidRecords() {
        long ret = countValidRecords();
        return lines.size() - ret;
    }

}
