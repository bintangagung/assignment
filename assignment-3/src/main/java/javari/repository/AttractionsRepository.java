package javari.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AttractionsRepository {
    private static ArrayList<String> datas = new ArrayList<>();

    public static void save(String data) {
        datas.add(data);
    }

    public static List<String> findAttractionsBySpecies(String species) {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            if (data.split(",")[0].equals(species)) {
                ret.add(data.split(",")[1]);
            }
        }
        return new ArrayList<>(ret);
    }

    public static List<String> findAllAttractions() {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            ret.add(data.split(",")[1]);
        }
        return new ArrayList<>(ret);
    }

    public static long findNumberOfAttractions() {
        return findAllAttractions().size();
    }
}
