﻿Dokumen ini berisi deskripsi dan informasi umum yang diperlukan untuk
menyelesaikan tugas pertama kursus Dasar Pemrograman 2. Itu
Tugas adalah tentang membuat sebuah program untuk stasiun kereta (imajiner)
manajer yang membantu masyarakat lokal untuk mengirim spesies kucing yang terancam punah ke Javari
Taman konservasi kebun binatang.

Tugas ini mencakup topik pemrograman sebagai berikut:

Pengolahan Dasar I / O, khususnya penanganan standar input dan standar
keluaran
Konstruksi bersyarat dan iterasi
Rekursi
Pemrograman OO dasar di Jawa, secara khusus menerapkan kelas data sederhana
dengan atribut dan metode publik


Daftar Isi


Latar Belakang
Deskripsi
Persiapan
Menjalankan & Menguji
pengajuan
Daftar periksa


Latar Belakang

Selamat datang di Kebun Binatang konservasi Javari Park!
Kucing liar yang digambarkan di atas adalah salah satu Tomcat terkenal yang tinggal di Javari
Taman. Biasanya mereka berkeliaran dengan kucing liar lain bernama Servlet dan
bersama-sama mereka bisa membuat aplikasi berbasis web. Keren banget kan
Tidak jarang menemukan kucing, atau hewan lain, yang bisa kode di Javari Park.
Salah satu jenis ular bernama Python juga mampu membuat aplikasi Web dengan
membalik Flask atau membaca mantra dari buku yang disebut Django.
Sayangnya, bukan itu masalahnya di dunia tempat Anda tinggal. Anda tidak akan menemukannya
kucing yang bisa kode Tapi dalam beberapa kesempatan saat bintang dan planet itu ada
Selaras, ada satu tempat bernama Tamfir dimana binatang, terutama kucing,
menjadi sadar diri dan mampu menulis kode.
Karena sangat jarang kucing yang bisa menulis kode, pastilah begitu
diawetkan dan dijaga agar terhindar dari rebus. Untuk melakukannya, kucing
akan diangkut ke Javari Park dengan menggunakan kereta khusus. Tugasmu sebagai
seorang programmer adalah untuk membantu menerapkan sistem pemuatan kereta api di Kereta Tamfir
Stasiun yang akan menerima kucing dan memuatnya ke dalam kereta.


Deskripsi

Anda diminta mengembangkan program Java bernama A1Station yang akan digunakan
untuk mengelola mobil kereta dan kucing yang akan dimasukkan ke dalam kereta. Program
menggunakan dua kelas untuk mewakili konsep yang terlibat dalam masalah. Pertama
kelas, WildCat, mewakili kucing yang akan diangkut ke Javari Park.
Kelas kedua, TrainCar, mewakili mobil di kereta api yang mungkin terkait
mobil lain di kereta yang sama dan juga berisi sebuah instance dari WildCat.

Kode starter tersedia di direktori src / main / java. Tolong pastikan semuanya
dari pekerjaan Anda disimpan dalam folder yang sama dengan kode starter.
Subbagian berikut menggambarkan spesifikasi masing-masing kelas.


Kelas WildCat
Teks seperti tabel berikut adalah diagram kelas berbasis teks yang menjadi model
Kelas WildCat:

|          WildCat           |
|----------------------------|
| - name : String            |
| - weight : double          |
| - length : double          |
|----------------------------|
| + WildCat(name : String,   |
|           weight : double, |
|           length : double) |
| + computeMassIndex() :     |
|   double                   |
|----------------------------|

Diagram kelas menentukan variabel dan metode contoh berikut
ada di kelas WildCat:
Variabel dengan tipe String untuk menyimpan nama kucing
Variabel dengan tipe double untuk menyimpan berat kucing kilogram
Variabel dengan tipe double untuk menyimpan panjang kucing sentimeter
Metode konstruktor yang menerima tiga argumen dengan jenis berikut di
order: String, double, double

Metode publik bernama computeMassIndex () yang mengembalikan nilai dengan tipe
melipatgandakan dan menghitung Indeks Massa Tubuh (BMI) berdasarkan berat kucing
dan panjang

Rumusnya adalah sebagai berikut: BMI = berat (kg) / panjang (m) kuadrat

- selanjutnya: TrainCar

|           TrainCar          |
|-----------------------------|
| - cat : WildCat             |
| - next : TrainCar           |
|-----------------------------|
| + TrainCar(cat : WildCat)   |
| + TrainCar(cat : WildCat,   |
|           next : TrainCar)  |
| + computeTotalWeight() :    |
|   double                    |
| + computeTotalMassIndex() : |
|   double                    |
| + printCar() : void         |
|-----------------------------|
Diagram kelas menentukan variabel dan metode contoh berikut
ada di kelas TrainCar:

Sebuah variabel dengan tipe WildCat untuk menyimpan kucing yang akan dibawa oleh mobil ini
Variabel dengan tipe TrainCar untuk menyimpan referensi ke mobil berikutnya di melatih
Dua metode konstruktor: konstruktor pertama menerima satu argumen dengan
ketik WildCat, yang terakhir menerima dua argumen dengan tipe WildCat dan TrainCar

Metode publik bernama computeTotalWeight () dengan tipe double dua kali lipat
menghitung berat total mobil ini dan isinya ditambah berat total dari
mobil berikutnya (s), rekursif

Metode publik bernama computeTotalMassIndex () dengan tipe double return
Itu menghitung total indeks massa tubuh kucing di mobil ini plus
setiap kucing (s) dari mobil berikutnya (s), rekursif

Nama umum printCar () yang menampilkan konten mobil ini
dan link ke mobil berikutnya jika mobil ini bukan mobil terakhir di kereta,
secara rekursif

Spesifikasi: Jika saat ini objek TrainCar tidak terhubung dengan yang lain
mobil (yaitu mobil tunggal), printCar () harus mencetak string "(<CAT>)"
dimana <CAT> adalah nama kucing yang ada di kereta api saat ini. Jika tidak,
print string "(<CAT>) -".


Kelas Program A1Station

Program A1Station akan menerima input dari input standar (misal via keyboard)
dan tampilkan output ke output standar (yaitu command prompt / shell). Itu
Baris pertama yang diberikan dari input akan menjadi nilai integer tunggal, N, yang menentukan
Jumlah kucing yang akan diproses oleh program. N baris berikutnya
dari input akan berisi string yang menggambarkan setiap kucing dan diformat sebagai
mengikuti:
<NAME>, <WEIGHT>, <LENGTH>
Saat memproses setiap baris input, program harus bisa mengurai
masukan dan instantiate objek kelas WildCat berdasarkan parsing
informasi. Kemudian, kucing itu perlu dimasukkan ke dalam mobil. Namun,
Program tidak bisa begitu saja memasukkan kucing ke mobil yang ada. Jika ada
Mobil yang ada di lintasan, buat mobil baru untuk menampung kucing dan buat
Mobil baru memiliki link ke mobil terbaru di lintasan.

# Sebelum
mobil di lintasan: (Mobil N) - (Mobil N-1) - ...
# Setelah
mobil di lintasan: (Mobil N + 1) - (Mobil N) - ...
Jika tidak ada mobil di lintasan, maka buatlah mobil baru untuk menampung kucing
dan menugaskannya ke trek sebagai mobil pertama di lintasan.

# Sebelum
mobil di lintasan: TIDAK ADA
# Setelah
mobil di lintasan: (Mobil 1)

Petunjuk: Anda harus memiliki variabel yang terus melacak mobil terbaru di
jalur. Mobil terbaru memiliki informasi tentang kucing N-th dan
mobil sebelumnya Mobil sebelumnya memiliki informasi tentang kucing N-1-th
dan mobil sebelumnya sebelumnya. Dan seterusnya.

Petunjuk: perhatikan pola itu? Singkatnya, sebuah kereta didefinisikan secara rekursif di mana
sebuah kereta terdiri dari dirinya sendiri (yaitu mobil tunggal) atau diikuti oleh yang lain
kereta (mobil).

Setelah sebuah mobil baru ditambahkan ke trek dan dihubungkan dengan mobil yang ada (jika ada),
Anda perlu memeriksa apakah berat total semua mobil yang ada di kereta api
melebihi ambang batas (yaitu 250 kg). Jika berat total telah terlampaui
ambang batas, lalu lepaskan kereta, kosongkan lintasannya, dan terus mengolahnya
sisa masukan Bila tidak ada masukan lebih lanjut (yaitu sudah sampai pada akhir
masukan) dan ada kereta di lintasan, segera berangkat kereta.

1. Kapan pun kereta berangkat, program akan menampilkan informasi berikut:
Serangkaian string yang berbunyi: "Kereta berangkat ke Javari Park"
Sederet string yang menampilkan kucing di mobil sebagai teks
Format: "[LOCO] <- (Cat di mobil N) - (Cat di mobil N-1) --...- (Cat di mobil 1)"
Contoh 1: "[LOCO] <- (DELTA) - (CHARLIE) - (BOB) - (ALICE)"
Contoh 2: "[LOCO] <- (ALICE)"
Petunjuk: Ingat metode printCar () dalam objek TrainCar? Anda bisa menggunakannya
untuk rekursif mencetak setiap mobil!

2. Dua baris string yang menggambarkan statistik tentang indeks massa rata-rata semua
kucing di kereta
Baris pertama adalah string yang berbunyi:
"Rata-rata indeks massa semua kucing:" diikuti dengan yang sebenarnya
dihitung indeks massa rata-rata
Baris kedua adalah string yang berbunyi:
"Rata-rata, kucing di kereta api sedang <CATEGORY> *" di mana <CATEGORY>
memiliki empat nilai yang mungkin: berat badan kurang jika indeks massa rata-rata kurang
dari 18,5, normal jika indeks massa rata-rata lebih besar dari atau sama
menjadi 18,5 dan kurang dari 25, kelebihan berat badan jika indeks massa rata-rata adalah
lebih besar dari atau sama dengan 25 dan kurang dari 30, obesitas jika rata-rata
Indeks massa lebih besar dari atau sama dengan 30.

Petunjuk: Demikian pula, Anda dapat menggunakan metode computeTotalMassIndex () di
Objek TrainCar mendapatkan indeks massa total.