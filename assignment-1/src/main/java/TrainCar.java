public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    // TODO Complete me!
	// atribut
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat; // kucing yang diangkut
        this.next = next; // objek sebelum train car
    }

    public double computeTotalWeight() {
        // TODO Complete me!
		// Rekursif untuk mendapatkan berat
        double totalWeight = EMPTY_WEIGHT;
        if (next == null){ //null-> sesuatu yang belum ada nilainya
            totalWeight += cat.weight;
        } else{
            totalWeight += cat.weight + next.computeTotalWeight(); // rekursif
        }
        return totalWeight;
    }

    public double computeTotalMassIndex() {
		// TODO Complete me!
		// Rekursif untuk mendapatkan BMI
        double totalMassIndex = 0;
        if (next == null){
            totalMassIndex += cat.computeMassIndex();
        } else{
            totalMassIndex = cat.computeMassIndex() + next.computeTotalMassIndex();
        }
        return totalMassIndex;
    }

    public void printCar() {
        // TODO Complete me!
        if (next == null){
            System.out.println ("("+ cat.name +")"); // sesuai template
        } else{
            System.out.print("("+ cat.name +")--");
            next.printCar(); // rekursif
        }
    }
}