import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    // You can add new variables or methods in this class
    public static void main(String[] args) {
        // TODO Complete me!
		// Main
        Scanner input = new Scanner (System.in);
        int n = Integer.parseInt(input.nextLine()); // jumlah inputan kucing
        WildCat[] cat = new WildCat[n]; // array cat baru
        TrainCar[] train = new TrainCar[n]; // array cat kereta
        int count = 0;

        for (int i=0; i<n; i++){ // looping kucing
			// memasukan kucing-kucing
            String[] masukan = input.nextLine().split(",");
            cat[i] = new WildCat(masukan[0], Double.parseDouble(masukan[1]), Double.parseDouble(masukan[2]));
            count +=1;
            if (count==1){
                train[i] = new TrainCar(cat[i]); // jika cukup satu kereta
            } else{
                train[i] = new TrainCar(cat[i], train[i-1]); // menambahkan kereta
            }
            double average = train[i].computeTotalMassIndex() / (double) count; // rumus rata-rata

            if(train[i].computeTotalWeight() >= THRESHOLD || (i == n-1)){ // maksimal 250kg
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                train[i].printCar(); // memanggil output
                System.out.println("Average mass index of all cats: "+average);
                String category = "";
				// menentukan kategori
                if(average < 18.5){
                    category = "*underweight*";
                } else if(average < 25){
                    category = "*normal*";
                } else if(average < 30){
                    category = "*overweight*";
                } else {
                    category = "*obese*";
                }
                System.out.println("In average, the cats in the train are "+category);
            count = 0;
			}
        }
        input.close();
    }
}